;;; nmcli.el --- The nmcli interface  -*- lexical-binding: t; -*-

;; Copyright 2023 Christian Gimenez
;;
;; Author: Christian Gimenez
;; Maintainer: Christian Gimenez
;; Version: 0.1.0
;; Keywords: tools
;; URL: https://gitlab.com/cnngimenez/nmcli-el
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Interface to manage network connections through nmcli.

;;; Code:

(require 'seq)
(require 'ert)

(defgroup nmcli nil
  "The nmcli interface."
  :group 'applications)

(defcustom nmcli-default-type-icon "❓"
  "Icon used when the type is not in `nmcli-type-icons'."
  :type 'string
  :group 'nmcli)

(defcustom nmcli-type-icons
  '(("wireless" . "🛜")
    ("ethernet" . "🔌")
    ("vpn" . "🛡️")
    ("loopback" . "🔁"))
  "Icons used to represent the type of connection.
The connectino type is tested with `string-suffix-p', because the
usual string returnde by nmcli is in the form of STANDARD-TYPE.
For example: \"802-3-ethernet\"."
  :type '(alist :key-type string :value-type string)
  :group 'nmcli)
  

(defun nmcli--parse-connections (str)
  "Parse a line from the terse output of nmcli connections.
STR is a string with the line to parse.
Return an alist with the connection data."
  (seq-filter
   (lambda (connection)
     (not (equal (alist-get 'name connection) "")))
              
   (mapcar (lambda (connection)
             (let ((data (split-string connection ":")))
               (list (cons 'name (nth 0 data))
                     (cons 'uuid (nth 1 data))
                     (cons 'type (nth 2 data))
                     (cons 'device (nth 3 data)))))
           (split-string str "\n"))))

(ert-deftest nmcli--parse-connections-test ()
  (should (equal (nmcli--parse-connections "Conn1:12345679-abcd-1234-abcd-567890123456:802-11-wireless:wlo1
lo:abcdef12-1234-abcd-1234-abcdef123456:loopback:lo
Conn2:98765432-4321-7654-bcdef1234567890ab:802-3-ethernet:

")
                 '(((name . "Conn1")
                    (uuid . "12345679-abcd-1234-abcd-567890123456")
                    (type . "802-11-wireless")
                    (device . "wlo1"))
                   ((name . "lo")
                    (uuid . "abcdef12-1234-abcd-1234-abcdef123456")
                    (type . "loopback")
                    (device . "lo"))
                   ((name . "Conn2")
                    (uuid . "98765432-4321-7654-bcdef1234567890ab")
                    (type . "802-3-ethernet")
                    (device . ""))))))

(defun nmcli--list-connections ()
  "Call nmcli to retrieve all connections and parse it.
Return a list of alists with the connections and their data."
  (with-current-buffer (get-buffer-create "*nmcli-process*")
    (erase-buffer)
    (call-process "nmcli" nil "*nmcli-process*" nil "--terse" "c")
    (nmcli--parse-connections (buffer-substring-no-properties (point-min)
                                                             (point-max)))))

(defun nmcli-connect (&optional connection-name)
  "Set up a connection.
CONNECTION-NAME is a string with the connection to set up.  If not
provided, ask the user with autocompletion."
  (interactive (list (completing-read "Which connection?"
                                      (mapcar (lambda (x)
                                                (alist-get 'name x))
                                              (nmcli--list-connections))
                                      nil t)))

  (with-current-buffer (get-buffer-create "*nmcli-up*")
    (erase-buffer)
    (call-process "nmcli" nil "*nmcli-up*" t "c" "up" connection-name)
    (switch-to-buffer (current-buffer))))


(defun nmcli-disconnect (&optional connection-name)
  "Set down a connection.
CONNECTION-NAME is a string with the connection to disconnect.  If not
provided, ask the user with autocompletion."
  (interactive (list (completing-read "Which connection?"
                                      (mapcar (lambda (x)
                                                (alist-get 'name x))
                                              (nmcli--list-connections))
                                      nil t)))

  (with-current-buffer (get-buffer-create "*nmcli-down*")
    (erase-buffer)
    (call-process "nmcli" nil "*nmcli-up*" t "c" "down" connection-name)
    (switch-to-buffer (current-buffer))))

(defun nmcli-show-connection (&optional connection-name)
  "Show all information about a connection.
CONNECTION-NAME is a string with the connection to show.  If not
provided, ask the user with autocompletion."
  (interactive (list (completing-read "Which connection?"
                                      (mapcar (lambda (x)
                                                (alist-get 'name x))
                                              (nmcli--list-connections))
                                      nil t)))

  (with-current-buffer (get-buffer-create "*nmcli-show*")
    (erase-buffer)
    (call-process "nmcli" nil "*nmcli-show*" t "c" "show" connection-name)
    (goto-char (point-min))
    (switch-to-buffer (current-buffer))))

(defun nmcli--type-icon (type)
  "Return the character or emoji string representing the connection.
TYPE is the connection type parsed from nmcli."
  (alist-get type nmcli-type-icons
             nmcli-default-type-icon
             nil
             #'string-suffix-p))

(defun nmcli--device-str (device)
  "Return a string representing the connected device.
DEVICE is the device string or an empty string if disconnected."
  (cond ((string= device "")
         "Disconnected")
        (t device)))

(defun nmcli--insert-connection (connection)
  "Insert the connection data in the current buffer.
CONNECTION must be the parsed data (an alist of connection data)."
  (insert (propertize (format "%s %50s %s\n"
                              (nmcli--type-icon (alist-get 'type connection))
                              (alist-get 'name connection)
                              (nmcli--device-str (alist-get 'device connection)))
                      'nmcli-connection-name (alist-get 'name connection))))

(defun nmcli-list-connections ()
  "List all connections in a buffer."
  (interactive)
  (with-current-buffer (get-buffer-create "*nmcli-connections*")
    (let ((inhibit-read-only t))
      (erase-buffer)
      (mapc #'nmcli--insert-connection (nmcli--list-connections))
      (nmcli-connections-mode)
      (switch-to-buffer (current-buffer))
      (goto-char (point-min)))))

(defun nmcli-status ()
  "Show the interface status."
  (interactive)
  (with-current-buffer (get-buffer-create "*nmcli-status*")
    (let ((inhibit-read-only t))
      (erase-buffer)
      (call-process "nmcli" nil "*nmcli-status*" t "--terse")
      (switch-to-buffer (current-buffer)))))

(defvar nmcli-connections-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "c" #'nmcli-connect-at-point)
    (define-key map "d" #'nmcli-disconnect-at-point)
    (define-key map "i" #'nmcli-show-connection-at-point)
    (define-key map "g" #'nmcli-list-connections)
    map)
  "Keymap for `nmcli-connection-mode'.")

(define-derived-mode nmcli-connections-mode special-mode
  "nmcli-connections"
  "Major mode for `nmcli-list-connections'.
Keybindings:
\\{nmcli-connections-mode-map}"

  (use-local-map nmcli-connections-mode-map)
  (setq buffer-read-only t))
  
(defun nmcli-connect-at-point ()
  "Connect to the connection name pointend at current position."
  (interactive)
  (let ((connection-name (get-text-property (point) 'nmcli-connection-name)))
    (if connection-name
        (nmcli-connect connection-name)
      (message "No connection name found at point."))))

(defun nmcli-disconnect-at-point ()
  "Disconnect from the connection name pointend at current position."
  (interactive)
  (let ((connection-name (get-text-property (point) 'nmcli-connection-name)))
    (if connection-name
        (nmcli-disconnect connection-name)
      (message "No connection name found at point."))))

(defun nmcli-show-connection-at-point ()
  "Disconnect from the connection name pointend at current position."
  (interactive)
  (let ((connection-name (get-text-property (point) 'nmcli-connection-name)))
    (if connection-name
        (nmcli-show-connection connection-name)
      (message "No connection name found at point."))))

(provide 'nmcli)

;;; nmcli.el ends here
